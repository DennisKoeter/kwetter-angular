import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() profileName;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  logout(){
    localStorage.setItem("loggedInUser", "");
    localStorage.setItem("loggedInUserId", "");
    localStorage.setItem("accesstoken", "");

    this.router.navigateByUrl('/');
  }

}
