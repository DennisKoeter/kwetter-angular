import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PostpageComponent} from './postpage/postpage.component';
import {ProfilepageComponent} from './profilepage/profilepage.component';
import {HomepageComponent} from './homepage/homepage.component';
import {ComposepageComponent} from './composepage/composepage.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'profile/:id', redirectTo: 'profile/:id/kweets'},
  {path: 'post/:id', component: PostpageComponent},
  {path: 'home', component: HomepageComponent},
  {path: 'compose', component: ComposepageComponent},
  {path: 'profile/:id/kweets', component: ProfilepageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'profile/:id/:type', component: ProfilepageComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
