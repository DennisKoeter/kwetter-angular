import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileName;
  profile;
  biography;
  username;
  fullName;
  joinDate;
  profilePicUrl;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.profileName = this.route.snapshot.paramMap.get('id');
    this.http.get('http://localhost:8080/user/'+this.profileName+'/profile', {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe(data => {
      this.profile = data;

      this.biography = this.profile.biography;
      this.username = this.profile.username;
      this.fullName = this.profile.fullName;
      this.profilePicUrl = this.profile.profilePicture;
      this.joinDate = this.profile.joinDate;
    });
  }

}
