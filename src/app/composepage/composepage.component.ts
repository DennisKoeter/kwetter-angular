import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-composepage',
  templateUrl: './composepage.component.html',
  styleUrls: ['./composepage.component.css']
})
export class ComposepageComponent implements OnInit {
  loggedInUser = localStorage.getItem("loggedInUser");

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  kweet(kweetText){
    this.http.post("http://localhost:8080/kweet/create", {"user": this.loggedInUser, "content": kweetText}, {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe();
  }

}
