import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposepageComponent } from './composepage.component';

describe('ComposepageComponent', () => {
  let component: ComposepageComponent;
  let fixture: ComponentFixture<ComposepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComposepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComposepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
