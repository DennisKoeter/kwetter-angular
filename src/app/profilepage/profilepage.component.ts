import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profilepage',
  templateUrl: './profilepage.component.html',
  styleUrls: ['./profilepage.component.css']
})
export class ProfilepageComponent implements OnInit {
  profileName;
  timeline;
  type;
  pageCounter = 0;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    this.profileName = this.route.snapshot.paramMap.get('id');
    this.type = this.route.snapshot.paramMap.get('type');
    switch (this.type) {
      case 'kweets-and-replies':
        this.http.get('http://localhost:8080/user/' + this.profileName + '/kweets_and_replies', {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) this.timeline = data['timeline'];
        });
        break;
      case 'kweets-with-media':
        this.http.get('http://localhost:8080/user/' + this.profileName + '/kweets_with_media', {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) this.timeline = data['timeline'];
        });
        break;
      case 'following':
        break;
      case 'followers':
        break;
      case 'likes':
        this.http.get('http://localhost:8080/user/' + this.profileName + '/likes', {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) this.timeline = data['timeline'];
        });
        break;
      default:
        this.http.get('http://localhost:8080/user/' + this.profileName + '/kweets', {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) this.timeline = data['timeline'];
        });
        break;
    }
  }

  loadMore(){
    this.pageCounter++;
    switch (this.type) {
      case 'kweets-and-replies':
        this.http.get('http://localhost:8080/user/' + this.profileName + '/kweets_and_replies?page='+this.pageCounter, {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) {
            for(let i = 0; i < data['timeline'].length; i++){
              this.timeline.push(data['timeline'][i]);
            }
          }
        });
        break;
      case 'kweets-with-media':
        this.http.get('http://localhost:8080/user/' + this.profileName + '/kweets_with_media?page='+this.pageCounter, {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) {
            for(let i = 0; i < data['timeline'].length; i++){
              this.timeline.push(data['timeline'][i]);
            }
          }
        });
        break;
      case 'following':
        break;
      case 'followers':
        break;
      case 'likes':
        this.http.get('http://localhost:8080/user/' + this.profileName + '/likes?page='+this.pageCounter, {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) {
            for(let i = 0; i < data['timeline'].length; i++){
              this.timeline.push(data['timeline'][i]);
            }
          }
        });
        break;
      default:
        this.http.get('http://localhost:8080/user/' + this.profileName + '/kweets?page='+this.pageCounter, {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem("accesstoken"),
            'userId': localStorage.getItem("loggedInUserId")
          })
        }).subscribe(data => {
          if(data) {
            for(let i = 0; i < data['timeline'].length; i++){
              this.timeline.push(data['timeline'][i]);
            }
          }
        });
        break;
    }
  }
}
