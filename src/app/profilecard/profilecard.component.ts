import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profilecard',
  templateUrl: './profilecard.component.html',
  styleUrls: ['./profilecard.component.css']
})
export class ProfilecardComponent implements OnInit {
  profileName;
  profileCard;
  username;
  fullName;
  profilePicUrl;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.profileName = this.route.snapshot.paramMap.get('id');
    if(!this.profileName){
      this.profileName = localStorage.getItem("loggedInUser"); //TODO: handle this case better
    }
    this.http.get('http://localhost:8080/user/'+this.profileName+'/card',{
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe(data => {
      this.profileCard = data;
      this.username = this.profileCard.username;
      this.fullName = this.profileCard.fullName;
      this.profilePicUrl = this.profileCard.profilePicture;
    });
  }

}
