import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  loggedInUser = localStorage.getItem("loggedInUser");

  timeline;
  pageCounter = 0;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    if(!localStorage.getItem('accesstoken')){
      // window.location.reload();
    }
    this.http.get('http://localhost:8080/user/' + this.loggedInUser + '/timeline', {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe(data => {
      this.timeline = data['timeline'];
    });
  }

  loadMore() {
    this.pageCounter++;
    this.http.get('http://localhost:8080/user/' + this.loggedInUser + '/timeline?page=' + this.pageCounter,{
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe(data => {
      if (data) {
        for (let i = 0; i < data['timeline'].length; i++) {
          this.timeline.push(data['timeline'][i]);
        }
      }
    });
  }
}
