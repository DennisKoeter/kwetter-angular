import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  //TODO: likes, replies and rekweets counter
  loggedInUser = localStorage.getItem("loggedInUser");

  @Input() post;

  type;
  postid;
  content;
  username;
  fullname;
  profilepicture;
  rekweetinguser;
  likinguser;
  time;
  quotes;
  hasQuotes=false;
  media = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.type = this.post.type;

    switch (this.type){
      case "kweet":
        this.postid = this.post.kweetId;
        this.content = this.post.content;
        this.username = this.post.user.username;
        this.fullname = this.post.user.fullName;
        this.profilepicture = this.post.user.profilePicture;
        this.quotes = this.post.quotes;
        this.time = this.post.timestamp;
        this.media = this.post.media;
        break;
      case "rekweet":
        this.postid = this.post.rekweetedKweet.kweetId;
        this.content = this.post.rekweetedKweet.content;
        this.username = this.post.rekweetedKweet.user.username;
        this.fullname = this.post.rekweetedKweet.user.fullName;
        this.profilepicture = this.post.rekweetedKweet.user.profilePicture;
        this.rekweetinguser = this.post.rekweetingUser.username;
        this.quotes = this.post.rekweetedKweet.quotes;
        this.time = this.post.rekweetedKweet.timestamp;
        this.media = this.post.rekweetedKweet.media;
        break;
      case "like":
        this.postid = this.post.likedKweet.kweetId;
        this.content = this.post.likedKweet.content;
        this.username = this.post.likedKweet.user.username;
        this.fullname = this.post.likedKweet.user.fullName;
        this.profilepicture = this.post.likedKweet.user.profilePicture;
        this.likinguser = this.post.likingUser.username;
        this.quotes = this.post.likedKweet.quotes;
        this.time = this.post.likedKweet.timestamp;
        this.media = this.post.likedKweet.media;
        break;
    }
  }

  rekweet(){
    this.http.post("http://localhost:8080/user/"+this.loggedInUser+"/rekweet/"+this.postid, null, {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe();
  }

  like(){
    this.http.post("http://localhost:8080/user/"+this.loggedInUser+"/like/"+this.postid, null, {
      headers: new HttpHeaders({
        'Authorization': localStorage.getItem("accesstoken"),
        'userId': localStorage.getItem("loggedInUserId")
      })
    }).subscribe();
  }
}
