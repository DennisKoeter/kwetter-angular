import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient,
              private router: Router) {
  }

  ngOnInit() {
  }

  login(username: String) {
    localStorage.setItem("loggedInUser", "");
    localStorage.setItem("loggedInUserId", "");
    this.http.get("http://localhost:8080/user/"+username+"/logindata").subscribe(data => {
      localStorage.setItem("loggedInUser", data['username']);
      localStorage.setItem("loggedInUserId", data['userId']);

      this.http.post(
        "http://localhost:6969/security/token",
        {"username": data['username'], "userId": data['userId']})
        .subscribe(data => {
          localStorage.setItem("accesstoken", data['token']);
        });

      setTimeout((router: Router) => {
        this.router.navigate(['/home']);
      }, 50);
    });
  }

}
