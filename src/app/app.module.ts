import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProfilepageComponent } from './profilepage/profilepage.component';
import { ProfilecardComponent } from './profilecard/profilecard.component';
import { ProfileComponent } from './profile/profile.component';
import { PostComponent } from './post/post.component';
import { PostpageComponent } from './postpage/postpage.component';
import { AppRoutingModule } from './/app-routing.module';
import {RouteReuseStrategy, RouterModule} from '@angular/router';
import {AARouteReuseStrategy} from './AARouteReuseStrategy';
import { HeaderComponent } from './header/header.component';
import { ComposepageComponent } from './composepage/composepage.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    ProfilepageComponent,
    ProfilecardComponent,
    ProfileComponent,
    PostComponent,
    PostpageComponent,
    HeaderComponent,
    ComposepageComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: AARouteReuseStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
