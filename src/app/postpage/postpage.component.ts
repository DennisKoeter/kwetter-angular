import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-postpage',
  templateUrl: './postpage.component.html',
  styleUrls: ['./postpage.component.css']
})
export class PostpageComponent implements OnInit {
  post;
  kweetId;
  allDataLoaded = false;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    this.kweetId = this.route.snapshot.paramMap.get('id');
    this.http.get('http://localhost:8080/kweet/'+this.kweetId).subscribe(data => {
      this.post = data;
      this.allDataLoaded = true;
    });
  }
}
